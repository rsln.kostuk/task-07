package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
	private static final String URL;

	static {
		Properties props = new Properties();
		try {
			props.load(new FileInputStream("app.properties"));
		} catch(FileNotFoundException ex) {
			System.out.println(ex.getMessage());
		} catch(IOException ex) {
			System.out.println("Couldn't load properties:");
			System.out.println(ex.getMessage());
		}
		URL = props.getProperty("connection.url");
	}

	private static final DBManager instance = new DBManager();

	public static synchronized DBManager getInstance() {
		return instance;
	}

	private DBManager() {}

	public List<User> findAllUsers() throws DBException {
		final String query = "SELECT id as id, login as login FROM users";
		final List<User> userList = new ArrayList<>();
		try(final Connection con = DriverManager.getConnection(URL);
			final Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(query)) {
			while(rs.next()) {
				final int id = rs.getInt("id");
				final String login = rs.getString("login");
				final User user = new User();
				user.setId(id);
				user.setLogin(login);
				userList.add(user);
			}
		} catch(SQLException ex) {
			printExceptionInfo(ex);
		}
		return userList;
	}

	public boolean insertUser(User user) throws DBException {
		final String insertQuery = "INSERT INTO users(login) VALUES('" + user.getLogin() + "')";
		final String selectQuery = "SELECT id as id FROM users WHERE login = '" + user.getLogin() + "'";
		try(Connection con = DriverManager.getConnection(URL);
			Statement stmt = con.createStatement()) {
			final int inserted = stmt.executeUpdate(insertQuery);
			final ResultSet rs = stmt.executeQuery(selectQuery);
			if(rs.next()) {
				final int id = rs.getInt(1);
				user.setId(id);
			}
			return inserted > 0;
		} catch(SQLException ex) {
			printExceptionInfo(ex);
			throw new DBException("Couldn't insert user!", ex);
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		final String query = "DELETE FROM users WHERE login = ?";
		int[] rowsUpdated = null;
		try(final Connection con = DriverManager.getConnection(URL);
			final PreparedStatement stmt = con.prepareStatement(query)) {
			for(User u : users) {
				stmt.setString(1, u.getLogin());
				stmt.addBatch();
			}
			rowsUpdated = stmt.executeBatch();
		} catch(SQLException ex) {
			printExceptionInfo(ex);
		}
		return rowsUpdated != null && Arrays.stream(rowsUpdated).anyMatch(i -> i > 0);
	}

	public User getUser(String login) throws DBException {
		final String query = "SELECT id as id, login as login FROM users WHERE login = '" + login + "'";
		User resultUser = null;
		try(final Connection con = DriverManager.getConnection(URL);
			final Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(query)) {
			if(rs.next()) {
				final int id = rs.getInt("id");
				resultUser = new User();
				resultUser.setId(id);
				resultUser.setLogin(login);
			}
		} catch(SQLException ex) {
			printExceptionInfo(ex);
		}
		return resultUser;
	}

	public Team getTeam(String name) throws DBException {
		final String query = "SELECT id as id, name as name FROM teams WHERE name = '" + name + "'";
		Team resultTeam = null;
		try(final Connection con = DriverManager.getConnection(URL);
			final Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(query)) {
			if(rs.next()) {
				final int id = rs.getInt("id");
				resultTeam = new Team();
				resultTeam.setId(id);
				resultTeam.setName(name);
			}
		} catch(SQLException ex) {
			printExceptionInfo(ex);
		}
		return resultTeam;
	}

	public List<Team> findAllTeams() throws DBException {
		final String query = "SELECT id as id, name as name FROM teams";
		final List<Team> teamList = new ArrayList<>();
		try(final Connection con = DriverManager.getConnection(URL);
			final Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(query)) {
			while(rs.next()) {
				final int id = rs.getInt("id");
				final String name = rs.getString("name");
				final Team team = new Team();
				team.setId(id);
				team.setName(name);
				teamList.add(team);
			}
		} catch(SQLException ex) {
			printExceptionInfo(ex);
		}
		return teamList;
	}

	public boolean insertTeam(Team team) throws DBException {
		final String updateQuery = "INSERT INTO teams(name) VALUES ('" + team.getName() + "')";
		final String selectQuery = "SELECT id as id FROM teams WHERE name = '" + team.getName() + "'";
		try(final Connection con = DriverManager.getConnection(URL);
			final Statement stmt = con.createStatement()) {
			final int inserted = stmt.executeUpdate(updateQuery);
			final ResultSet rs = stmt.executeQuery(selectQuery);
			if(rs.next()) {
				final int id = rs.getInt(1);
				team.setId(id);
			}
			return inserted > 0;
		} catch(SQLException ex) {
			printExceptionInfo(ex);
			throw new DBException("Couldn't insert team!", ex);
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		final String updateQuery = "INSERT INTO users_teams(user_id, team_id) VALUES (" + user.getId() + ", ?)";
		int[] rowsUpdated = null;
		Connection con = null;
		try {
			con = DriverManager.getConnection(URL);
			con.setAutoCommit(false);
			final PreparedStatement stmt = con.prepareStatement(updateQuery);
			for(Team t : teams) {
				stmt.setInt(1, t.getId());
				stmt.addBatch();
			}
			rowsUpdated = stmt.executeBatch();
			con.commit();
		} catch(SQLException ex) {
			if(con != null) {
				try {
					con.rollback();
				} catch(SQLException innerEx) {
					printExceptionInfo(innerEx);
				}
			}
			printExceptionInfo(ex);
			throw new DBException("Transaction failed!", ex);
		} finally {
			if(con != null) {
				try {
					con.close();
				} catch(SQLException innerEx) {
					printExceptionInfo(innerEx);
				}
			}
		}
		return rowsUpdated != null && Arrays.stream(rowsUpdated).anyMatch(i -> i > 0);
	}

	public List<Team> getUserTeams(User user) throws DBException {
		final String query =
				"SELECT t.id as id, t.name as name FROM teams t " +
						"LEFT JOIN users_teams ut ON ut.team_id = t.id WHERE ut.user_id = " + user.getId();
		final List<Team> teamList = new ArrayList<>();
		try(final Connection con = DriverManager.getConnection(URL);
			final Statement stmt = con.createStatement();
			final ResultSet rs = stmt.executeQuery(query)) {
			while(rs.next()) {
				final int id = rs.getInt("id");
				final String name = rs.getString("name");
				final Team tmp = new Team();
				tmp.setId(id);
				tmp.setName(name);
				teamList.add(tmp);
			}
		} catch(SQLException ex) {
			printExceptionInfo(ex);
		}
		return teamList;
	}

	public boolean deleteTeam(Team team) throws DBException {
		final String updateQuery = "DELETE FROM teams WHERE id = " + team.getId();
		try(Connection con = DriverManager.getConnection(URL);
			Statement stmt = con.createStatement()) {
			return stmt.executeUpdate(updateQuery) > 0;
		} catch(SQLException ex) {
			printExceptionInfo(ex);
			throw new DBException("cannot delete team!", ex);
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		final String updateQueryPattern = "UPDATE teams SET name = ? WHERE id = ?";
		try(Connection con = DriverManager.getConnection(URL);
			PreparedStatement stmt = con.prepareStatement(updateQueryPattern)) {
			stmt.setString(1, team.getName());
			stmt.setInt(2, team.getId());
			return stmt.executeUpdate() > 0;
		} catch(SQLException ex) {
			printExceptionInfo(ex);
			throw new DBException("cannot delete team!", ex);
		}
	}

	private static void printExceptionInfo(SQLException ex) {
		System.err.println(ex.getMessage());
		System.err.println("Error Code: " + ex.getErrorCode());
		System.err.println("SQL State: " + ex.getSQLState());
		ex = ex.getNextException();
		if(ex != null) {
			printExceptionInfo(ex);
		}
	}
}
